# GUI Git client

Design and implement GUI Git client application.
Language/libraries: C++, STL, usage of additional libraries or 3rd party code is prohibited.
For UI might be used Qt, for threading you may use platform-independent API.
Use MVC design pattern in your application.
The application should be written using OOP.
